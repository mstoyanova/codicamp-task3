<?php
	$page_title = 'Home';
	include('templates/header.php');
?>

<p class="slogan">Sticker Mule. Best Place For Your Sticker Needs!</p>
<div >
	<img class="float_left img1_blog_item" src="img/blog_item_page.png">
	<div>
		<table >
			<thead>
				<th class="table_stats_header">Date</th>
				<th class="table_stats_header">Tags</th>
				<th class="table_stats_header">Author</th>
			</thead>
			<tbody>
				<tr>
					<td class="table_stats">April 15, 2012</td>
					<td class="table_stats">Website | Design</td>
					<td class="table_stats">Michael Reimer</td>
				</tr>
			</tbody>
		</table>
		<p class="blog_item_text">Lorem ipsum dolor sit amet, mollis epicuri pri ei, perpetua honestatis ad vix. Ne duo ludus putent, cu causae tamquam voluptua duo. Agam officiis no duo, ut reque decore sea. Cu eripuit accusam vix. Facete blandit detraxit pri cu, sea soluta doming civibus ea.</br></br>
Eum eu tale clita iuvaret, cu est saperet forensibus interesset, cum ne case iusto oportere. Id idque indoctum eum, menandri mediocrem has ei. At usu modo quaerendum. Sit ei dicunt tacimates, mea ea enim eirmod suscipiantur, amet dicit ancillae vel in. Ex mea augue eloquentiam, his postea dolorem suavitate ea. Mel hendrerit accommodare concludaturque ex.</p>
		<img class="more_button" src="img/arrows.png">
		<p class="submit_button">Visit Website</p>
	</div>
	<img class="float_left img2_blog_item" src="img/blog_item_page2.png">
</div>

<p class="slogan clearfix">Similar Posts. Check Them Out!</p>
<section >

	<div id="one" class="column">
		<article class="post float_left">
			<img src="img/img_post6.png">
			<div class="post_text">
				<p class="related_post_title">Character Design</p>
				<p class="related_post_info">June 15, 2012 </p>					
			</div>
		</article>
	</div>
	<div id="two" class="column">
		<article class="post float_left">
			<img src="img/img_post3.png">
			<div class="post_text">
				<p class="related_post_title">Top iPhone Apps</p>
				<p class="related_post_info">June 15, 2012 </p>
			</div>
		</article>
	</div>
	<div id="three" class="column">
		<article class="post float_left">
			<img src="img/blog_item_page3.png">
			<div class="post_text">
				<p class="related_post_title">Social Media Buttons</p>
				<p class="related_post_info">June 15, 2012 </p>
			</div>
		</article>
	</div>
	<div  id="four" class="column">
		<article class="post float_left">
			<img src="img/blog_item_page4.png">
			<div class="post_text">
				<p class="related_post_title">10 Amazing Websites</p>
				<p class="related_post_info">June 15, 2012 </p>
			</div>
		</article>
	</div>
		</section>
<?php include('templates/footer.php') ?>