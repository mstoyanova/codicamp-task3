<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
	
	<p class="clearfix slogan">Read Out Our Latest Posts. Learn Something New Maybe!</p>
	<ul class="post_categories">
		<li class="red_color">ALL</li>
		<li>/</li>
		<li>NEWS</li>
		<li>/</li>
		<li>DESIGN</li>
		<li>/</li>
		<li>PRINT</li>
		<li>/</li>
		<li>ART</li>
		<li>/</li>
		<li>DEVELOPMENT</li>
	</ul>
		<section id="container">
			<div id="one" class="column">
				<article class="post">
					<img src="img/img_post1.png">
					<div class="post_text">
						<p class="post_title">Sticker Mule</p>
						<p class="post_info">June 15, 2012 / news, contests</p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post5.png">
					<div class="post_text">
						<p class="post_title">Big Buck Bunny</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post9.png">
					<div class="post_text">
						<p class="post_title">Pinterest Icons</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
			<div id="two" class="column">
				<article class="post">
					<img src="img/img_post2.png">
					<div class="post_text">
						<p class="post_title">10 Amazing Websites</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post6.png">
					<div class="post_text">
						<p class="post_title">Character Design</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post10.png">
					<div class="post_text">
						<p class="post_title">iPad 3 Review</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
			<div id="three" class="column">
				<article class="post">
					<img src="img/img_post3.png">
					<div class="post_text">
						<p class="post_title">Top iPhone Apps</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post7.png">
					<div class="post_text">
						<p class="post_title">Service Icons</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post11.png">
					<div class="post_text">
						<p class="post_title">Social Media Buttons</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
			<div id="four" class="column">
				<article class="post">
					<img src="img/img_post4.png">
					<div class="post_text">
						<p class="post_title">Photo Shoot</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post8.png">
					<div class="post_text">
						<p class="post_title">Wedding Card</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post12.png">
					<div class="post_text">
						<p class="post_title">Silver UI Kit</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
		</section>
		<section class="pagination clearfix">
			<button class="page_button">1</button>
			<button class="page_button page_selected">2</button>
			<button class="page_button">3</button>
			<button class="page_button">4</button>
		</section>
<?php include('templates/footer.php') ?>