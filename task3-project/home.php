	<div class="slider clearfi2">
		
		<img src="img/img_area1.png">
		<img src="img/img_area2.png">
		<img src="img/img_area3.png">
		<img src="img/img_area4.png">
		<img class="middle_row" src="img/img_area5.png">
		<img src="img/img_area6.png">
		<img src="img/img_area7.png">
		<img src="img/img_area8.png">
		<img src="img/img_area9.png">
	</div>

	<div class="clearfix overlay"><img class="overlay" src="img/img_area9-overlay.png"></div>
	<div class="clearfix blurb"><img src="img/blurb.png"></div>
	<div class="clearfix previous"><img src="img/previous.png"></div>
	<div class="clearfix next"><img src="img/next.png"></div>

	<p class="clearfix slogan">A Theme Unlike Any Other. Simply Fantastic!</p>


	<ul class="post_categories">
		<li class="red_color">ALL</li>
		<li>/</li>
		<li>NEWS</li>
		<li>/</li>
		<li>DESIGN</li>
		<li>/</li>
		<li>PRINT</li>
		<li>/</li>
		<li>ART</li>
		<li>/</li>
		<li>DEVELOPMENT</li>
	</ul>
		<section id="container">
			<div id="one" class="column">
				<a href="blog_item.php">
					<article class="post">
						<img src="img/img_post1.png">
						<div class="post_text">
							<p class="post_title">Sticker Mule</p>
							<p class="post_info">June 15, 2012 / news, contests</p>
							<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
						
								<img class="more_button" src="img/arrows.png">
								<p class="more_button">More</p>
							
						</div>
					</article>
				</a>

				<article class="post">
					<img src="img/img_post5.png">
					<div class="post_text">
						<p class="post_title">Big Buck Bunny</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post9.png">
					<div class="post_text">
						<p class="post_title">Pinterest Icons</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
			<div id="two" class="column">
				<article class="post">
					<img src="img/img_post2.png">
					<div class="post_text">
						<p class="post_title">10 Amazing Websites</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post6.png">
					<div class="post_text">
						<p class="post_title">Character Design</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post10.png">
					<div class="post_text">
						<p class="post_title">iPad 3 Review</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
			<div id="three" class="column">
				<article class="post">
					<img src="img/img_post3.png">
					<div class="post_text">
						<p class="post_title">Top iPhone Apps</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post7.png">
					<div class="post_text">
						<p class="post_title">Service Icons</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post11.png">
					<div class="post_text">
						<p class="post_title">Social Media Buttons</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
			<div id="four" class="column">
				<article class="post">
					<img src="img/img_post4.png">
					<div class="post_text">
						<p class="post_title">Photo Shoot</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post8.png">
					<div class="post_text">
						<p class="post_title">Wedding Card</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>

				<article class="post">
					<img src="img/img_post12.png">
					<div class="post_text">
						<p class="post_title">Silver UI Kit</p>
						<p class="post_info">June 15, 2012 </p>
						<p class="post_content">Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores. </p>
					
							<img class="more_button" src="img/arrows.png">
							<p class="more_button">More</p>
						
					</div>
				</article>
			</div>
		</section>

		<section class="pagination clearfix">
			<button class="page_button">1</button>
			<button class="page_button page_selected">2</button>
			<button class="page_button">3</button>
			<button class="page_button">4</button>
		</section>

