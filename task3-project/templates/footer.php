        <footer class="clearfix">
	        <div class="social_icons">
	    		<a href="https://twitter.com/"><img src="img/icon_twitter.png"></a>
	    		<a href="https://www.rss.com/"><img src="img/icon_rss.png"></a>
	    		<a href="https://www.facebook.com/"><img src="img/icon_facebook.png"></a>
	    		<a href="https://www.pinterest.com/"><img src="img/icon_pinterest.png"></a>
	    	</div>
	    	<div class="footer_text">
	    		<p id="text1">Best PSD Freebies | info@bestpsdfeebies.com</p>
	    		<p id="text2">Copyright 2012 Gridzilla Theme www.bestpsdfeebies.com</p>
	    	</div>
        </footer>
    </div><!-- End of wrapper -->
    <script src="js/libs/jquery-1.8.3.min.js"></script>
    <script>
        if (typeof jQuery == 'undefined') {
            var e = document.createElement('script');
            e.src = "js/libs/jquery-1.8.3.min.js";
            e.type='text/javascript';
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>
    <script src="js/application.js"></script>
</body>
</html>