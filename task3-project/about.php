<?php
	$page_title = 'Home';
	include('templates/header.php');
?>

<p class="slogan">About Us. Our Mission. Shortcodes. Tons More!</p>
<div>
	<p class="without_bottom_margin about_text3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis. 
	</p>
	<img class="float_left img_about" src="img/img_post4.png">
	<div class="img_about about_text1" >
		<p>Maecenas ipsum metus, semper hendrerit varius mattis, hasr congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elem entum enim rutrum. Nam mi erat, porta idso ultrices nec, pellentesq ue vel nunc. Cras varius fermentum iaculis. Aenean sodales nibh non lectus tempor a interdumni justo ultricies. Sed luctus dui nec ni  sl tem pus faucibus sit amet et sem. Aenean augue sapien, sodales ac bibendum ut, pellentesque id eros. Duis tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae. </p>
		<p>Pellentesque ultricies nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Maecenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Pellentesque ultricies nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci.  Aenean augue sapien, sodales ac bibendum ut, pellentesque id eros. Maecenas ipsum metus, semper hendrerit varius mattis, congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elementum enim rutrum. Nam mi erat, porta id ultrices nec, pellentesque vel nunc. Cras varius fermentum iaculis. Aenean sodales nibh non lectus tempor a interdum.</p>
	</div>
	<p class="clearfix about_text1" >Maecenas ipsum metus, semper hendrerit varius mattis, congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elementum enim rutrum. Nam mi erat, porta id ultrices nec, pellentesque vel nunc. Cras varius fermentum iaculis. Aenean sodales nibh non lectus tempor a interdum justo ultricies. Sed luctus dui nec nisl tempus faucibus sit amet et sem. Aenean augue sapien, sodales ac bibendum ut, pellentesque id eros.</p>
	<p class="about_text1">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Maecenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Duis tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae. </p>
	<p class="drop_caps float_left">D</p>
	<p class="about_text1">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed l           	         ectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Maecenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Duis tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae. Pellentesque ultricies nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci.</p>
	<p class="quotes float_left">"</p>
	<p class="about_text2">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Mae trenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Du tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae. Pellentesque ultric nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci donec sed nulla at sit amet.</p>
	<p class="clearfix without_bottom_margin float_left two_column_text about_text1">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. </p>
	<p class="without_right_margin without_bottom_margin float_left two_column_text about_text1">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. </p>
	
	<p class="without_bottom_margin clearfix float_left three_column_text about_text1">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas.</p>
	<p class="without_bottom_margin float_left three_column_text about_text1">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas.</p>
	<p class="without_bottom_margin without_right_margin float_left three_column_text about_text1 ">Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas.</p>
</div>

<section id="buttons">
	<input type="button" class="green" value="Button Text"></button>
	<input type="button" class="silver" value="Button Text"></button>
	<input type="button" class="blue" value="Button Text"></button>
	<input type="button" class="orange" value="Button Text"></button>
	<input type="button" class="purple" value="Button Text"></button>
	<input type="button" class="red" value="Button Text"></button>
	<input type="button" class="baby_blue" value="Button Text"></button>
</section>

<section id="alerts">
	<input class="green_alert" type="alert" value="Alert Text"></input>
	<input class="red_alert" type="alert" value="Alert Text"></input>
	<input class="orange_alert" type="alert" value="Alert Text"></input>
	<input class="blue_alert" type="alert" value="Alert Text"></input>
</section>

<?php include('templates/footer.php') ?>